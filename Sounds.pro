#-------------------------------------------------
#
# Project created by QtCreator 2014-02-14T10:12:24
#
#-------------------------------------------------

QT       += core gui
QT += multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Sounds
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    audiomanip.cpp

HEADERS  += mainwindow.h \
    wavesound.h \
    audiomanip.h

FORMS    += mainwindow.ui

OTHER_FILES +=

RESOURCES += \
    images.qrc
