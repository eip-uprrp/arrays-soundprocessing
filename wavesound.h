#ifndef WAVESOUND_H
#define WAVESOUND_H

#include <QtMultimedia/QAudioBuffer>
#include <QtMultimedia/QAudioFormat>
#include <QFile>
#include <iostream>
#include <QByteArray>

using namespace std ;

class WaveSound
{
    private:
        QByteArray header;
        QAudioFormat format;
        QByteArray wavBuffer;
        QAudioBuffer  *qab;

    public:

        WaveSound()
        {
            qab = NULL ;
            format.setSampleSize(16);
            format.setSampleRate(44100);
            format.setChannelCount(2);
            format.setCodec("audio/pcm");
            format.setByteOrder(QAudioFormat::LittleEndian);
            format.setSampleType(QAudioFormat::SignedInt);
        }

        QAudioBuffer::S16S * getAudioSamples(QString filename)
        {
            QFile inputFile;

            inputFile.setFileName(filename);
            inputFile.open(QIODevice::ReadOnly);
            header = inputFile.read(44) ;  // Wav header file is normally 44 bytes

            wavBuffer = inputFile.readAll();
            inputFile.close();

            if(qab != NULL){
                delete qab ;
            }
            qab = new QAudioBuffer(wavBuffer, format);

            return qab->data<QAudioBuffer::S16S>();
        }

        void writeNewAudioFile(QString filename)
        {
            QByteArray outBuffer;
            QFile outputFile;

            outputFile.setFileName(filename);
            outputFile.open(QIODevice::WriteOnly);
            outBuffer.setRawData((char *) qab->data(), qab->byteCount() );

            outputFile.write(header);
            outputFile.write(outBuffer);
            outputFile.close();
        }

        int frameCount()
        {
            return qab->frameCount();
        }


        ~WaveSound(){
            if(qab != NULL){
                delete qab ;
                qab = NULL;
            }
        }

};
#endif // WAVESOUND_H
